package io.ipfs.utils;

public enum Reachable {
    UNKNOWN, PUBLIC, PRIVATE
}
